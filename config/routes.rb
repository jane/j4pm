Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  get '/portfolio', to: 'svelte#portfolio'
  get '/friends', to: 'svelte#friends'
  root 'svelte#index'
end
