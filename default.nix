{pkgs ? import <nixpkgs> {}}:
with pkgs;
let
	ruby = ruby_3_0;
	gems = bundlerEnv {
    		name = "env";
    		inherit ruby;
			gemfile = ./Gemfile;
			lockfile = ./Gemfile.lock;
    		gemset = import ./gemset.nix;
	};
in
stdenv.mkDerivation {
    name = "janus";
    src = ./.;
    buildInputs = [gems gems.wrappedRuby yarn];
}
