import App from '../friends.svelte'

document.addEventListener('DOMContentLoaded', () => {
  const app = new App({
    target: document.body,
  });

  window.app = app;
})

