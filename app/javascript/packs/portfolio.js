import App from '../portfolio.svelte'

document.addEventListener('DOMContentLoaded', () => {
  const app = new App({
    target: document.body,
  });

  window.app = app;
})

